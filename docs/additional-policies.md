## Erweiterte Kyverno-Regeln (bzw. Ideen) aus dem BSI SYS.1.6

Anhand der Ausarbeitungen der IG BvC zum Container-Baustein SYS.1.6 wurden weitere mögliche Kyverno-Policies identifiziert.

### Aufbau:

- **Nr. Anforderung:** Konkrere Forderung aus SYS.1.6 ODER Anforderungen der IG BvC
  - Objekt: ... (In dieser Form wird eine konkrete Policy(-Idee) beschrieben, die auf Objekt wirkt)

### Konkrete Idee bereits vorhanden:

- **A5:** Plattformbetreiber MUSS sicherstellen, dass Master-Nodes keine Nutzer-Workloads ausführen dürfen.
  - Pod: Prüfung auf Vorhandensein von spec.nodeSelector.node-role.kubernetes.io/worker=* **ODER** spec.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution. ...
- **A17:** Wenn Schreibrechte nicht benötigt werden, SOLLTEN diese entfernt werden. 
  - PVC: Forcieren von Setzen eines AccessModes (Inhalt wird nicht geprüft)
- **A18:** Softwarebetreiber SOLLTE für jeden nach außen exponierten Service eine eigene IP-Adresse nutzen.
  - Service: **folgt.** Ähnlich der Unique-UID-Regel
- **A23:** Softwarebetreiber MUSS durch Netzwerk-Richtlinien (Network Policies) nur die für den Betrieb erforderlichen Netzwerkverbindungen zu Containern erlauben und für administrative Aufgaben typische Protokolle blockieren (z.B. SSH, Telnet, RDP, VNC).
  - NetworkPolicy: Freischalten von Ports, die für gewöhnlich für Fernwartungsprotokolle genutzt werden (z.B. Port 22), wird abgelehnt. (SSH, Telnet, RDP, VNC...?)
  - Technisch realisiert über Blockieren von Pattern spec.Ingress.ports.port=22 **UND** ...port=23 etc.
- **A25:** Container SOLLTEN jeweils eigene Service-Accounts nutzen, um miteinander und mit den Diensten der Cluster-Betriebssoftware authentifiziert zu kommunizieren.
  - Pod: Setzen von spec.serviceAccountName forcieren; spec.serviceAccountName="default" blockieren. (Default ServiceAccounts von Namespaces tragen immer den Namen default)
- **A25:** Jeder Dienst, der einen Service-Account nutzt, SOLLTE ein eigenes Token erhalten. 
  - Jeder Dienst sollte einen eigenen Service-Account erhalten, d.h. wenn Token jedes Service-Accounts einzigartig ist, benutzt jedes Deployment eigenen Token
  - ServiceAccount: Abfrage von genutzten Tokens (ähnlich Unique-UID-Policy)
- **A33:** Plattformbetreiber MUSS ein Regelwerk implementieren, dass alle Kommunikation ohne explizite Regel unterbindet.
  - Grundsätzlich sollte eine Default-Deny-NetworkPolicy, welche über einen offenen podSelector auf jeden Pod wirkt, jedoch keine Verbindungen freischaltet, in jedem Namespace implementiert werden. Existiert eine NetworkPolicy, die einen Pod auswählt, so kann dieser nur die freigeschalteten Verbindungen nutzen. Wird ein Pod von keiner NetworkPolicy ausgewählt, so kommuniziert er ohne Limitierungen. Diese NetworkPolicy sollte über ihren Namen klar identifizierbar sein (s. nächster Punkt).
  - Pod: Prüfung auf Vorhandensein von Default-Deny-Policy im Namespace des Pods -> Existiert keine Default-Deny-NetworkPolicy, kann der Namespace effektiv nicht genutzt werden. 
  - https://kyverno.io/policies/other/require_netpol/?policytypes=validate Anpassung auf Pods?



### Konkreter Plan zur Umsetzung fehlt; zur Diskussion
- **A16:** Für jeden Container SOLLTEN Ressourcen auf dem Host-System, wie CPU sowie flüchtiger und persistenter Speicher, angemessen limitiert werden. 
  - Es ist zu prüfen, ob mithilfe einer Kyverno-Regel Begrenzungen für Ressourcen-Requests und -Limits durchgesetzt werden sollten, um extrem große Zahlen, die ggf. durch Schreibfehler oder Unwissen entstehen, zu verhindern. 
  - Ressourcen unter anderem: Netzwerkdurchsatz (Trafficshaping), CPU, RAM
  - Auch möglich ist das Schreiben einer Regel, welche nicht im produktiven Umfeld, sondern nur im Dev genutzt wird.
- **A17:** Plattformbetreiber sollte die Größe der persistent Volume Claims begrenzen.
  - Technische Umsetzung der Regel relativ klar, Frage nach sinnvoller Begrenzung?
- **A17:** Sofern Container lokale Speicher einbinden, SOLLTEN die Zugriffsrechte im Dateisystem auf den Service-Account des Containers eingeschränkt sein. 
  - Frage nach praktischer Umsetzung?


### Weitere Ideen; bedarf weiterer Anpassung von Prozessen o.ä.
- **A6:** Plattformbetreiber SOLL automatisierte Policies implementieren, die die Herkunft der Images prüft und durchsetzt.
  - Prüfung der Signaturen von Images abhängig vom Stand der Technik beim Signieren von Images 
  - https://kyverno.io/docs/writing-policies/verify-images/ 
- **A7:** Die Konfiguration der Anwendung bzw. des Dienstes MUSS angemessen gehärtet werden.
  - Zu prüfen ist, inwieweit die Härtung über ENV-Variablen in ConfigMaps erreicht wird, und ob diese über Kyverno prüfbar sind / inwieweit das sinnvoll ist.
    - Extrem anwendungsspezifisch (Denkbar wäre z.B. bei Postgres Image: Setzen von Variable POSTGRES_PASSWORD mit weniger als 8 Zeichen ist verboten...?)
    - aufwändig, aber für oft genutzte Anwendungen / Images möglicherweise sinnvoll?
- **A13:** Plattformbetreiber muss die Freigabebedingungen in einem definierten Freigabeprozess prüfen und den Softwarebetreiber über die Ergebnisse des Freigabeprozesses informieren.
  - nach Etablierung eines Freigabeprozesses wäre eine entsprechende Annotation ("freigegeben") mit Kyverno prüfbar, sodass nur freigegebene Konfigurationsdateien genutzt werden können.
- **A22:** Alle Prozesse der Automatisierungssoftware SOLLTEN nur mit minimalen Rechten arbeiten. 
  - Um Automatisierungssoftware separat zu beschränken, wäre ein einheitliches Erkennungsmerkmal von Automatisierungs-Accounts nötig. Ein einheitlicher Name wäre hier denkbar. -> Anpassung Prozess
- **A24:** Plattformbetreiber MUSS dem Softwarebetreiber Accounts mit den erforderlichen Berechtigungen innerhalb dessen Namespaces bereitstellen. Die vergebenen Berechtigungen dürfen nur die unbedingt für den Betrieb der Container erforderlichen Berechtigungen umfassen (Least-Privelge). Plattformbetreiber MUSS die Vergabe von Accounts mit Cluster-weiten Berechtigungen zur Nutzung an den Softwarebetreiber ablehnen.
  - Generieren von Cluster-Rollen mit bestimmten Patterns / Stichwörtern / Attributen verbieten?
    - Wie sehen diese Patterns aus?
- **A34:** Generell Hochverfügbarkeit, (Geo-)Redundanz
  - Verteilen von Pods auf mehrere Standorte mittels Affinity / Anti-Affinity durch Kyverno prüfen?
    - Gegebenfalls nur bei Anwendungen mit hohem Anspruch an Verfügbarkeit (realisierbar durch "Hochverfügbarkeits-Label"? -> Anpassung Prozess)
