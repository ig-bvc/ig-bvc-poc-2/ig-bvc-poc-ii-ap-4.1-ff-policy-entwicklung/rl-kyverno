apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: require-health-and-liveness-check
  annotations:
    policies.opencode.de/ID: "012"
    policies.opencode.de/bsi-requirement: SYS.1.6.A11
    policies.kyverno.io/title: Require Pod Probes
    policies.kyverno.io/category: Best Practices
    policies.kyverno.io/severity: medium
    policies.kyverno.io/subject: Pod
    policies.kyverno.io/description: >-
      Liveness and readiness probes need to be configured to correctly manage a Pod's 
      lifecycle during deployments, restarts, and upgrades. For each Pod, a periodic 
      `livenessProbe` is performed by the kubelet to determine if the Pod's containers 
      are running or need to be restarted. A `readinessProbe` is used by Services 
      and Deployments to determine if the Pod is ready to receive network traffic.
      This policy validates that all containers have liveness and readiness probes by
      ensuring the `periodSeconds` field is greater than zero.      
  labels:
    policies.opencode.de/bsi-protection-requirement: standard
    policies.opencode.de/category: should
spec:
  background: true
  validationFailureAction: audit
  rules:
  - name: validate-livenessProbe-readinessProbe
    match:
      resources:
        kinds:
        - Pod
    validate:
      message: "Liveness and readiness probes are required. spec.containers[*].livenessProbe.periodSeconds must be set to a value greater than 0. "
      pattern:
        spec:
          containers:
          - livenessProbe:
              periodSeconds: ">0"      
            readinessProbe:
              periodSeconds: ">0"
